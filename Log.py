import sys
import itertools

spinner = itertools.cycle(['-', '/', '|', '\\'])
INFO = '\033[34m'
SUCCESS = '\033[32m'
WARNING = '\033[33m'
ERROR = '\033[31m'
ENDING = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'

_FILE = sys.stderr


def loading():
    _FILE.write(next(spinner))
    _FILE.flush()
    _FILE.write('\b')


def set_file(file):
    """
    Set the file to print the log messages to

    :param file: An open stream with write permissions
    """
    global _FILE
    _FILE = file


def warn(msg, end='\n'):
    """
    Print out a message in yellow.

    :param msg: String to print.

    :param end: line ending
    """
    print(BOLD + WARNING + msg + ENDING, end=end, file=_FILE)
    _FILE.flush()


def error(msg, end='\n'):
    """
    Print out a message in red.

    :param msg: String to print.

    :param end: line ending
    """

    print(BOLD + ERROR + msg + ENDING, end=end, file=_FILE)
    _FILE.flush()


def success(msg, end='\n'):
    """
    Print out a message in green.

    :param msg: String to print.

    :param end: line ending
    """
    print(BOLD + SUCCESS + msg + ENDING, end=end, file=_FILE)
    _FILE.flush()


def info(msg, end='\n'):
    """
    Print out a message in blue.

    :param msg: String to print.

    :param end: line ending
    """
    print(BOLD + INFO + msg + ENDING, end=end, file=_FILE)
    _FILE.flush()
