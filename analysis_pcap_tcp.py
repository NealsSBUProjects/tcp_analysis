#! /usr/bin/env python3
import argparse
import pcapy
import sys

import Log
from machines import Connections, Connection
from structs import IPHeader, TCPHeader, Packet

total_packet_count = 0
connections = Connections()


def pkt_handler(pkt_header, pkt_data):
    global total_packet_count, connections
    total_packet_count += 1
    packet = decode_packet(pkt_data)
    Log.loading()
    if packet.tcp.SYN and not packet.tcp.ACK:  # Beginning of connection
        connections[packet] = Connection(packet.ip.source_ip, packet.tcp.source_port,
                                         packet.ip.dest_ip, packet.tcp.dest_port)

    connections[packet].analyze(pkt_header, packet)


def decode_packet(pkt):
    pkt = pkt[14:]  # drop ethernet junk

    ihl = (pkt[0] & 0x0F) << 2  # Get packet length for the rare ip packets that have IP Option

    if ihl != 20:  # The avg ip packet length
        Log.warn('Header length is: {}'.format(ihl))

    ip = IPHeader.parse(pkt[:ihl])

    pkt = pkt[ip.header_length:]  # Toss parsed ip bytes

    tcp_sz = (((pkt[12] & 0xF0) >> 4) << 2)  # Tcp header size

    tcp = TCPHeader.parse(pkt[:tcp_sz])

    pkt = pkt[tcp.offset:]  # Toss parsed tcp bytes

    data = pkt

    return Packet(ip, tcp, data)


def main(args):
    Log.info('Parsing...')
    try:
        global total_packet_count, connections
        reader = pcapy.open_offline(args.FILE)
        reader.loop(-1, pkt_handler)  # PCAP only used for looping over packets
        Log.success(
            'EOF, {} Packets over {} Connections'.format(total_packet_count, len(connections)))
        for c in connections.values():
            c.info()
        print('\n')
    except pcapy.PcapError as ex:
        Log.error(ex)
        return 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser('analysis_pcap_tcp PCAP Analyzer')
    parser.add_argument('FILE', type=str, help='PCAP file to analyze')
    sys.exit(main(parser.parse_args()))
