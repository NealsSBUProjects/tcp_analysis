# TCP Analysis
#### Neal Beeken
##### CSE 534 - Computer Networks

## Dependencies
 - Construct
 ```commandline
 sudo -H pip3 install construct 
 ```
 - pcapy
 ```commandline
 sudo apt-get install python3-dev libpcap-dev
 sudo -H pip3 install pcapy
 ```
## How to run
```commandline
python3 analysis_pcap_tcp.py PCAP_FILE
python3 analysis_pcap_http.py PCAP_FILE
```
##### Usage
```text
usage: analysis_pcap_tcp/http PCAP Analyzer [-h] FILE

positional arguments:
  FILE        PCAP file to analyze

optional arguments:
  -h, --help  show this help message and exit
```