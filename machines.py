import struct
import socket

import copy

import Log

TRIGGER = 'trigger'
SOURCE = 'source'
DEST = 'dest'
TEMPLATE = '{0:42}{1:50}'
ALPHA = 0.875


class Queue(list):
    def enqueue(self, item):
        self.insert(0, item)

    def dequeue(self):
        return self.pop()

    def peek(self):
        return self[-1]


def ip_to_s(ip):
    return socket.inet_ntop(socket.AF_INET, struct.pack('!I', ip))


def pkt_header_to_s(header):
    return 'PacketHeader(ts={}, caplen={}, len={})'.format(header.getts()[1],
                                                           header.getcaplen(),
                                                           header.getlen())


class Connections(dict):
    def __getitem__(self, packet):
        """
        Pass in packet, will map to connection FSM and data.
        :param packet: packet returned by
        :return:
        """
        key = Connections.make_connection_tuple(packet)
        if key in self:
            return super().__getitem__(key)
        else:
            newkey = (key[1], key[0])
            if newkey in self:
                return super().__getitem__(newkey)
            else:
                raise KeyError

    def __setitem__(self, packet, value):
        key = Connections.make_connection_tuple(packet)
        super().__setitem__(key, value)

    def contains_connection_based_on_packet(self, packet):
        return Connections.make_connection_tuple(packet) in self

    @staticmethod
    def make_connection_tuple(packet):
        return (packet.ip.source_ip, packet.tcp.source_port), (packet.ip.dest_ip, packet.tcp.dest_port)


# Wow I'm dumb. dropping the server side simulation
class Connection:
    def __init__(self, source_ip, source_port, dest_ip, dest_port, title=None):
        self.source_ip = source_ip
        self.source_port = source_port
        self.dest_ip = dest_ip
        self.dest_port = dest_port
        self.first_ack_num = [-1, -1]
        self.first_seq_num = [-1, -1]
        self.first_rwnd_num = [-1, -1]
        self.pkt_count = 0
        self.unacked_pack = Queue()
        self.rtt = 0
        self.last_ack_num = 0
        self.dups = 0
        self.dup_pkt_ct = 0
        self.last_time = 0
        self.source_isn = 0
        self.dest_isn = 0
        self.estab_pkts = 0
        self.bytes = 0
        self.start_time = 0
        self.end_time = 0
        if title is None:
            title = '{}:{} -> {}:{}'.format(ip_to_s(source_ip), source_port,
                                            ip_to_s(dest_ip), dest_port)
        self.title = title

    def analyze(self, packet_header, packet):
        self.pkt_count += 1
        self.unacked_pack.enqueue(packet)
        self.bytes += packet.ip.total_length + 14
        if self.pkt_count <= 2:  # First Two Packets
            t = packet_header.getts()
            self.start_time = time_since_unix = t[0] + t[1] * (10 ** -6)
            self.rtt = time_since_unix - self.rtt
            self.last_time = time_since_unix
        else:
            t = packet_header.getts()
            time_since_unix = t[0] + t[1] * (10 ** -6)
            self.rtt = ALPHA * self.rtt + (1 - ALPHA) * (time_since_unix - self.last_time)

        if self.source_side(packet):
            if packet.tcp.SYN:  # We begin (First Packet out)
                self.source_isn = packet.tcp.seq_num
                self.unacked_pack.enqueue(packet)
            elif packet.tcp.ACK and not packet.tcp.FIN:  # The server is telling us what it got
                rel_ack = packet.tcp.ack_num - self.dest_isn
                rel_ack += 1
                if 5 > self.estab_pkts > 2:
                    self.first_ack_num[self.estab_pkts - 3] = copy.deepcopy(packet.tcp.ack_num)
                    self.first_seq_num[self.estab_pkts - 3] = copy.deepcopy(packet.tcp.seq_num)
                    self.first_rwnd_num[self.estab_pkts - 3] = copy.deepcopy(packet.tcp.window)
                self.estab_pkts += 1
            elif packet.tcp.FIN:
                Log.error('LastPacket')
                t = packet_header.getts()
                self.end_time = t[0] + t[1] * (10 ** -6)
        else:
            if packet.tcp.SYN and packet.tcp.ACK:  # We acknowledge the begin (First Packet in)
                self.dest_isn = packet.tcp.seq_num
            if packet.tcp.ACK:  # Acknowledging data sent
                rel_ack = packet.tcp.ack_num - self.source_isn
                if self.last_ack_num == packet.tcp.ack_num:
                    self.dups += 1
                    rel_ack += 1
                else:
                    self.dups = 0
                self.last_ack_num = packet.tcp.ack_num
                if self.dups >= 3:
                    self.dup_pkt_ct += 1
                    # Log.error('{}'.format(self.pkt_count))
                while self.unacked_pack.peek().tcp.seq_num < packet.tcp.seq_num:
                    self.unacked_pack.dequeue()  # Toss it's been ack'd!

    def source_side(self, packet):
        return packet.ip.source_ip == self.source_ip

    def __hash__(self):
        return hash(((self.source_ip, self.source_port), (self.dest_ip, self.dest_port)))

    def __eq__(self, other):
        return self.source_ip == other.source_ip and self.source_port == other.source_port \
               and self.dest_ip == other.dest_ip and self.dest_port == other.dest_port

    def __repr__(self, *args, **kwargs):
        return '<{}>'.format(self.title)

    def info(self):
        table = '\t{0:20} | {1:30}'
        Log.success('\n' + self.title + '\n')
        Log.success(table.format(*['Packet Count', self.pkt_count]))
        Log.success(table.format(*['Round Trip Time', self.rtt]))
        Log.success(table.format(*['Duplicate Packets', self.dup_pkt_ct]))
        Log.success(table.format(*['First Two ACK Num', str([hex(num) for num in self.first_ack_num])]))
        Log.success(table.format(*['First Two SEQ Num', str([hex(num) for num in self.first_seq_num])]))
        Log.success(table.format(*['First Two RWD Num', str([hex(num) for num in self.first_rwnd_num])]))
        Log.success(table.format(*['Loss Rate', '{} / {} = {}'.format(self.dup_pkt_ct,
                                                                      self.pkt_count,
                                                                      self.dup_pkt_ct / self.pkt_count)]))
        Log.success(table.format(*['Loss Rate', '{} / ({} - {}) = {} bytes/sec'.format(self.bytes, self.end_time, self.start_time, int(self.bytes / (self.end_time - self.start_time)))]))
