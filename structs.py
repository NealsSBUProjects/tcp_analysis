from construct import *
import collections

#   0                   1                   2                   3
#   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |Version|  IHL  |Type of Service|          Total Length         |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |         Identification        |Flags|      Fragment Offset    |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |  Time to Live |    Protocol   |         Header Checksum       |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                       Source Address                          |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                    Destination Address                        |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                    Options                    |    Padding    |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

IPHeader = Struct(
    EmbeddedBitStruct(
        "version" / Const(Nibble, 4),
        "header_length" / ExprAdapter(Nibble,
                                      encoder=lambda ihl, context: ihl >> 2,
                                      decoder=lambda ihl, context: ihl << 2),
    ),
    "tos" / BytesInteger(1),
    "total_length" / Int16ub,
    "identification" / Int16ub,
    EmbeddedBitStruct(
        "x" / Flag,
        "D" / Flag,
        "M" / Flag,
        "frag_offset" / BitsInteger(13)
    ),
    "ttl" / BytesInteger(1),
    "protocol" / Const(BytesInteger(1), 6),
    "header_checksum" / Int16ub,
    "source_ip" / Int32ub,
    "dest_ip" / Int32ub,
    "ip_option" / GreedyBytes
)

# TCP HEADER
#    0                   1                   2                   3
#    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |          Source Port          |       Destination Port        |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |                        Sequence Number                        |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |                    Acknowledgment Number                      |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |  Data |           |U|A|P|R|S|F|                               |
#   | Offset| Reserved  |R|C|S|S|Y|I|            Window             |
#   |       |           |G|K|H|T|N|N|                               |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |           Checksum            |         Urgent Pointer        |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |                    Options                    |    Padding    |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |                             data                              |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

TCPHeader = Struct(
    "source_port" / Int16ub,
    "dest_port" / Int16ub,
    "seq_num" / Int32ub,
    "ack_num" / Int32ub,
    EmbeddedBitStruct(
        "offset" / ExprAdapter(Nibble,
                               encoder=lambda offset, context: offset >> 2,
                               decoder=lambda offset, context: offset << 2),
        "reserved" / Nibble,
        "CWR" / Flag,
        "ECE" / Flag,
        "URG" / Flag,
        "ACK" / Flag,
        "PSH" / Flag,
        "RST" / Flag,
        "SYN" / Flag,
        "FIN" / Flag
    ),
    "window" / Int16ub,
    "checksum" / Int16ub,
    "urgent_ptr" / Int16ub,
    "tcp_option" / GreedyBytes
)

Packet = collections.namedtuple('Packet', 'ip tcp data')
